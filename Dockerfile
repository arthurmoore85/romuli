###################################################
#                ROMULI DOCKERFILE                #
#                                                 #
# This file will generate Docker container        #
# to run a containerized Romuli instance.         #
#                                                 #
# Author: Arthur Moore <arthur.moore85@gmail.com> #
###################################################

# This image uses Python 2.7. This is intentional.
# Since Romuli is aimed at the RetroPie which comes built
# with Python 2.7 (currently), I have opted to remain true
# to that. However, the code is entirely Py3 compatible,
# However, please note that Django version 2+ has some
# significant changes. So if you do use Python3 and you
# upgrade Django, be prepared to fix some issues.
FROM python:2.7

# Set environment variables
ENV PYTHONUNBUFFERED 1

# Set the container working directory.
WORKDIR /romuli

# Copy the Python requirements file to the container
ADD requirements.txt /romuli

# Install Python requirements.
RUN pip install -r requirements.txt

# Install PuDB and IPython for debugging. Remove if required.
RUN pip install pudb ipython

# Add the code to the container.
ADD . /romuli