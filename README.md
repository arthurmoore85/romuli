Romuli v.0.0.1
==============

RetroPie ROM manager and ROM downloader.

First there was Romulus, the RetroPie ROM manager for your desktop.
Romulus worked by connecting to your RetroPie and displaying your collection on a nice Qt app.
The issue with Romulus was that it required a computer.
Then came Pi Romulus, a ROM downloader that lived on your RetroPie and allowed you to easily
download ROMs from a graphical terminal app.
But after years of service, both Romulus and Pi Romulus combined forces to give you an even
better experience, and thus Romuli was born.

Romuli allows you to have the best of both apps.
It features the following (strike-outs are not yet available but planned):

* ~~Manage your collection~~
* ~~Download new ROMs from the app~~
* ~~View your achievements (using RetroAchievements)~~

But the best part is that it is accessible on many different devices.
For example, if you install Romuli on your RetroPie, you can access it directly on your Pi, access it
from your computer, or mobile device. You have full control over your collection, even whilst you are soaking
in the bath.

Let's go over the features:

Manage your collection (not yet implemented)
--------------------------------------------

With Romuli you are able to manage your RetroPie collection.

View your games, organised per system, name, or year. You can remove a game from your collection, add your own 
newly aquired ROM, change the name, or maybe just read more information.

Download new ROMs (not yet implemented)
---------------------------------------

The Pi Romulus app was useful in that it allowed you to download new ROMs without having to trawl lots of sites for it.

Romuli inherits this ability. On Romuli you can simply enter the name of a game, add some filters and search. Once
you found that game you like, simply download it, and it will even unzip (if required) and place it in the correct
directory. All you will need to do is play.

View your achievements (not yet implemented)
--------------------------------------------

If you have set up RetroAchievements on your RetroPie, you will be able to see your achievements straight from 
Romuli. If you select Super Mario World from the manager, you will also see which achievements you still have to
get.
