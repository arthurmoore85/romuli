"""
..module: games.py
..description: Games model
..author: Arthur Moore <arthur.moore85@gmail.com>
"""
from __future__ import unicode_literals

from django.db import models

from .systems import System
from ..utils.game_utils import GAME_GENRES


class Game(models.Model):
    """
    Game model
    """
    GAME_GENRES_TUPLE = ((v['id'], k) for k, v in GAME_GENRES.items())
    name = models.CharField(
        verbose_name="Game title",
        max_length=150
    )
    systems = models.ManyToManyField(
        System,
        verbose_name="Systems"
    )
    developer = models.CharField(
        verbose_name="Developer",
        max_length=150,
        null=True,
        blank=True
    )
    description = models.TextField(
        max_length=500,
        verbose_name="Game description",
        blank=True,
        null=True
    )
    genre = models.IntegerField(
        verbose_name="Genre",
        choices=GAME_GENRES_TUPLE,
        null=True,
        blank=True
    )
    year = models.IntegerField(
        verbose_name="Release year",
        null=True,
        blank=True
    )
    
    
    def __unicode__(self):
        return "<{name}>".format(name=self.name)
