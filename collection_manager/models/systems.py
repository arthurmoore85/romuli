"""
..module: systems.py
..description: Systems model
..author: Arthur Moore <arthur.moore85@gmail.com>
"""
from __future__ import unicode_literals

from django.db import models


class System(models.Model):
    """
    System model.
    """
    name = models.CharField(
        verbose_name="System name",
        max_length=100
    )
    company = models.CharField(
        verbose_name="Company",
        max_length=100
    )
    year = models.IntegerField(
        verbose_name="Release year"
    )

    def __unicode__(self):
        return "<{name}>".format(name=self.name)
