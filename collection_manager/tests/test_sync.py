"""
..module: test_sync.py
..description: Unittests for sync module (utils)

..author: Arthur Moore <arthur.moore85@gmail.com>
"""
import os
import shutil
from django.test import TestCase

from collection_manager.utils.sync import Sync
from romuli.settings import BASE_DIR


class SyncTestCase(TestCase):
    def setUp(self):
        self.test_files = {
            'megadrive': ['megadrive1.bin', 'megadrive2.bin'],
            'snes': ['snes1.bin', 'snes2.bin'],
        }
        os.makedirs(os.path.join(BASE_DIR, 'temp_roms'))
        self.path = os.path.join(BASE_DIR, 'temp_roms')
        for k, v in self.test_files.items():
            os.makedirs(os.path.join('temp_roms', k))
            for test_file in v:
                open(os.path.join(self.path, k, test_file), 'w')

    def tearDown(self):
        shutil.rmtree(self.path)

    def test_no_path(self):
        """
        Tests the Sync class when no path is selected.
        """
        with self.assertRaises(TypeError):
            s = Sync()

    def test_wrong_path(self):
        """
        Tests the Sync class when a path is incorrect.
        """
        s = Sync(library_path='/hello')
        result = s.fetch_roms()
        self.assertEquals(s._roms['errors'], 1)

    def test_fetch_roms(self):
        """
        Tests the Sync class fetch roms.
        """
        s = Sync(self.path)
        result = s.fetch_roms()
        self.assertTrue(s._roms['roms'])
