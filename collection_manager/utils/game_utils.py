"""
..module: .game_utils.py
..description: Utilities and global variables for Game objects.

..author: Arthur Moore <arthur.moore85@gmail.com>
..date: 16/06/2018
"""
from __future__ import unicode_literals

GAME_GENRES = {
    "Action": {
        'url': "Action", 
        'id': 1
    },
    "Action-Adventure": {
        'url': "Action-Adventure", 
        'id': 2
    },
    "Adventure": {
        'url': "Adventure", 
        'id': 3
    },
    "Baseball": {
        'url': "Baseball", 
        'id': 4
    },
    "Basketball": {
        'url': "Basketball", 
        'id': 5
    },
    "Billiards": {
        'url': "Billiards", 
        'id': 6
    },
    "Block-Breaking": {
        'url': "Block-Breaking", 
        'id': 7
    },
    "Bowling": {
        'url': "Bowling", 
        'id': 8
    },
    "Boxing": {
        'url': "Boxing", 
        'id': 9
    },
    "Brawler": {
        'url': "Brawler", 
        'id': 10
    },
    "Card Game": {
        'url': "CardGame", 
        'id': 11
    },
    "Compilation": {
        'url': "Compilation", 
        'id': 12
    },
    "Cricket": {
        'url': "Cricket", 
        'id': 13
    },
    "Driving/Racing": {
        'url': "DrivingRacing", 
        'id': 14
    },
    "Educational": {
        'url': "Educational", 
        'id': 15
    },
    "First-Person Shooter": {
        'url': "FPS", 
        'id': 16
    },
    "Fighting": {
        'url': "Fighting", 
        'id': 17
    },
    "Fishing": {
        'url': "Fishing", 
        'id': 18
    },
    "Fitness": {
        'url': "Fitness", 
        'id': 19
    },
    "Flight Simulator": {
        'url': "FlightSimulator", 
        'id': 20
    },
    "Football": {
        'url': "Football", 
        'id': 21
    },
    "Gambling": {
        'url': "Gambling", 
        'id': 22
    },
    "Golf": {
        'url': "Golf", 
        'id': 23
    },
    "Hockey": {
        'url': "Hockey", 
        'id': 24
    },
    "Light-Gun Shooter": {
        'url': "Light-GunShooter", 
        'id': 25
    },
    "MMORPG": {
        'url': "MMORPG", 
        'id': 26
    },
    "Minigame Collection": {
        'url': "MinigameCollection", 
        'id': 27
    },
    "Music/Rhythm": {
        'url': "MusicRhythm", 
        'id': 28
    },
    "Pinball": {
        'url': "Pinball", 
        'id': 29
    },
    "Platformer": {
        'url': "Platformer", 
        'id': 30
    },
    "Puzzle": {
        'url': "Puzzle", 
        'id': 31
    },
    "Role-playing": {
        'url': "RPG", 
        'id': 32
    },
    "Real-Time Strategy": {
        'url': "RTS", 
        'id': 33
    },
    "Shoot-em Up": {
        'url': "Shoot-Em-Up", 
        'id': 34
    },
    "Shooter": {
        'url': "Shooter", 
        'id': 35
    },
    "Simulation": {
        'url': "Simulation", 
        'id': 36
    },
    "Skateboarding": {
        'url': "Skateboarding", 
        'id': 37
    },
    "Snowboarding/Skiing": {
        'url': "SnowboardingSkiing", 
        'id': 38
    },
    "Soccer": {
        'url': "Soccer", 
        'id': 39
    },
    "Sports": {
        'url': "Sports", 
        'id': 40
    },
    "Strategy": {
        'url': "Strategy", 
        'id': 41
    },
    "Surfing": {
        'url': "Surfing", 
        'id': 42
    },
    "Tennis": {
        'url': "Tennis", 
        'id': 43
    },
    "Text Adventure": {
        'url': "TextAdventure", 
        'id': 44
    },
    "Track & Field": {
        'url': "TrackField", 
        'id': 45
    },
    "Trivia/Board Game": {
        'url': "TriviaBoardGame", 
        'id': 46
    },
    "Vehicular Combat": {
        'url': "VehicularCombat", 
        'id': 47
    },
    "Wrestling": {
        'url': "Wrestling", 
        'id': 48
    }
}
