"""
..module: sync.py
..description: Classes and functions to handle an existing
               library of games.

..author: Arthur Moore <arthur.moore85@gmail.com>
"""
from __future__ import unicode_literals

import os


class Sync(object):
    def __init__(self, library_path):
        """
        Class which handles syncing a pre-existing
        library of games.
        :param library_path: Path to game library (None)
        """
        self.library_path = library_path
        self._roms = {
            'roms': {},
            'errors': 0,
            'error_messages': None
        }

    def fetch_roms(self):
        """
        Returns a dictionary of games mapped to system.
        """
        if not os.path.exists(self.library_path):
            self._roms['errors'] = 1
            self._roms['error_messages'] = 'Path invalid'
            return self._roms
        
        results = {}
        
        direct_dirs = next(os.walk(self.library_path))[1]
        for sub in direct_dirs:
            results[sub] = []
            current_path = os.path.join(self.library_path, sub)
            for item in next(os.walk(current_path))[2]:
                results[sub].append(item)

        self._roms['roms'] = results
        return self._roms

            